package com.shen.day67_springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day67SpringBootApplication {

    public static void main(String[] args) {

        SpringApplication.run(Day67SpringBootApplication.class, args);
    }

}
